# (c) copyright 2023-2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gdk
from gi.repository import Gtk
from gi.repository import GLib
from kusozako_clock.const import ConfigKeys
from kusozako_clock.config.Config import KusozakoConfig
from . import ShadowType

CSS_REPLACEMENTS = {
    ConfigKeys.FOREGROUND_COLOR: "@KUSOZAKO_FOREGROUND_COLOR@",
    ConfigKeys.BACKGROUND_COLOR: "@KUSOZAKO_BACKGROUND_COLOR@",
    ConfigKeys.SHADOW_COLOR: "@KUSOZAKO_SHADOW_COLOR@",
}


class KusozakoCss:

    def _get_raw_data(self):
        names = [GLib.path_get_dirname(__file__), "application.css"]
        path = GLib.build_filenamev(names)
        _, bytes_ = GLib.file_get_contents(path)
        return bytes_.decode("utf-8")

    def _set_shadow_type(self, config, raw_data):
        shadow_type = config[ConfigKeys.SHADOW_TYPE]
        css = ShadowType.get_css(shadow_type)
        raw_data = raw_data.replace("@KUSOZAKO_SHADOW_TYPE", css)
        return raw_data

    def _get_css_data(self):
        raw_data = self._get_raw_data()
        config = KusozakoConfig.get_default()
        for key, place_holder in CSS_REPLACEMENTS.items():
            color = config[key]
            raw_data = raw_data.replace(place_holder, color)
        raw_data = self._set_shadow_type(config, raw_data)
        return bytes(raw_data, "utf-8")

    def __init__(self):
        css_provider = Gtk.CssProvider()
        css_data = self._get_css_data()
        css_provider.load_from_data(css_data)
        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(),
            css_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
            )

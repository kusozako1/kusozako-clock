# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from gi.repository import Gtk
from kusozako_clock import APPLICATION_ID
from kusozako_clock.Entity import DeltaEntity
from .window.Window import DeltaWindow
from .config.Config import KusozakoConfig
from .css.Css import KusozakoCss

FLAGS = (
    Gio.ApplicationFlags.NON_UNIQUE | Gio.ApplicationFlags.HANDLES_COMMAND_LINE
    )

CONFIG_OPTION = (
    "config",
    ord("c"),
    GLib.OptionFlags.NONE,
    GLib.OptionArg.STRING,
    "config file",
    None
)


class DeltaApplication(Gtk.Application, DeltaEntity):

    def _on_activate(self, application, config_path=None):
        KusozakoConfig.init(config_path)
        KusozakoCss()
        DeltaWindow(self)

    def _on_command_line(self, application, command_line):
        options = command_line.get_options_dict()
        value = options.lookup_value("config")
        config_path = None if value is None else value.get_string()
        self.connect("activate", self._on_activate, config_path)
        self.activate()
        return 0

    def _delta_call_add_to_container(self, widget):
        self.add_window(widget)

    def __init__(self):
        self._parent = None
        Gtk.Application.__init__(
            self,
            application_id=APPLICATION_ID,
            flags=FLAGS,
            )
        self.add_main_option(*CONFIG_OPTION)
        self.connect("command-line", self._on_command_line)

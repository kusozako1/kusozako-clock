# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_clock.Entity import DeltaEntity
from .image.Image import DeltaImage
from .metadata.Metadata import EchoMetadata


class DeltaAppeared(Gtk.Box, DeltaEntity):

    @classmethod
    def new(cls, parent, item_data):
        instance = cls(parent)
        instance.construct(item_data)

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def construct(self, item_data):
        is_horizontal = item_data.get("orientation", "") == "horizontal"
        if is_horizontal:
            self.set_orientation(Gtk.Orientation.HORIZONTAL)
        DeltaImage.new(self, item_data)
        EchoMetadata(self, item_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.VERTICAL,
            hexpand=True,
            spacing=8,
            )
        user_data = self, "appeared"
        self._raise("delta > add to stack", user_data)

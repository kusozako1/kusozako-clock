# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_clock.Entity import DeltaEntity
from kusozako_clock.const import PlayerControlSignals


class DeltaGetCoverart(DeltaEntity):

    def _callback(self, glib_variant):
        user_data = PlayerControlSignals.COVERART_CHANGED, glib_variant
        self._raise("delta > player control signal", user_data)

    def receive_transmission(self, user_data):
        signal, _ = user_data
        if signal != PlayerControlSignals.PROXY_APPEARED:
            return
        user_data = "GetCoverart", self._callback
        self._raise("delta > call method", user_data)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register player control object", self)

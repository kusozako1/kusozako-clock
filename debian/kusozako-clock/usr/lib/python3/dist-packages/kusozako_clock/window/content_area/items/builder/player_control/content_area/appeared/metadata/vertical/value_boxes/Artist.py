# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .ValueBox import AlfaValueBox


class DeltaArtist(AlfaValueBox):

    __header__ = "Artist : "
    __index__ = 1

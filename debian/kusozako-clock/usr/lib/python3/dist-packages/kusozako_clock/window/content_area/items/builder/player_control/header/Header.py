# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_clock.Entity import DeltaEntity
from kusozako_clock.bravo.Margin import BravoMargin
from .buttons.Buttons import EchoButtons


class DeltaHeader(Gtk.Box, DeltaEntity, BravoMargin):

    @classmethod
    def new(cls, parent, item_data):
        istance = cls(parent)
        istance.construct(item_data)

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def construct(self, item_data):
        self._set_margin(item_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(
            self,
            hexpand=True,
            orientation=Gtk.Orientation.HORIZONTAL,
            margin_start=8,
            margin_end=8,
            )
        self.set_size_request(-1, 32)
        label = Gtk.Label("Now Playing", xalign=0, hexpand=True)
        self.add(label)
        EchoButtons(self)
        self._raise("delta > add to container", self)
        self._raise("delta > add to container", Gtk.Separator())

# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_clock.Transmitter import FoxtrotTransmitter
from kusozako_clock.Entity import DeltaEntity
from .header.Header import DeltaHeader
from .content_area.ContentArea import DeltaContentArea


class DeltaHololive(DeltaEntity):

    @classmethod
    def new(cls, parent, item_data):
        instance = cls(parent)
        instance.construct(item_data)

    def _delta_call_hololive_signal(self, user_data):
        self._transmitter.transmit(user_data)

    def _delta_call_register_hololive_object(self, object_):
        self._transmitter.register_listener(object_)

    def construct(self, item_data):
        DeltaContentArea.new(self, item_data)

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = FoxtrotTransmitter()
        DeltaHeader(self)

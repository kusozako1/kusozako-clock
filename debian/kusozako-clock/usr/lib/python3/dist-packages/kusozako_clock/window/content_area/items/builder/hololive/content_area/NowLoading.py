# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_clock.Entity import DeltaEntity
from kusozako_clock.const import HololivePages


class DeltaNowLoading(Gtk.Label, DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(
            self,
            label="NOW LOADING",
            hexpand=True,
            vexpand=True,
            )
        user_data = self, HololivePages.NOW_LOADING
        self._raise("delta > add to stack", user_data)

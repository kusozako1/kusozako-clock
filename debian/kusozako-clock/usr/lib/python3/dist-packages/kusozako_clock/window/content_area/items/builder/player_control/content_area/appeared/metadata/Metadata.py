# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .horizontal.Horizontal import DeltaHorizontal
from .vertical.Vertical import DeltaVertical


class EchoMetadata:

    def __init__(self, parent, item_data):
        if item_data.get("orientation", "") == "horizontal":
            DeltaHorizontal(parent)
        else:
            DeltaVertical(parent)

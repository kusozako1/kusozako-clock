# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GtkLayerShell
from kusozako_clock import APPLICATION_ID
from kusozako_clock.const import ConfigKeys
from kusozako_clock.config.Config import KusozakoConfig
from kusozako_clock.Entity import DeltaEntity

EDGES = {
    "top": GtkLayerShell.Edge.TOP,
    "bottom": GtkLayerShell.Edge.BOTTOM,
    "left": GtkLayerShell.Edge.LEFT,
    "right": GtkLayerShell.Edge.RIGHT,
}


class DeltaLayerShell(DeltaEntity):

    def _set_edge(self, window):
        config = KusozakoConfig.get_default()
        for edge_position in config[ConfigKeys.EDGE]:
            GtkLayerShell.set_anchor(window, EDGES[edge_position], True)

    def _on_realize(self, window):
        GtkLayerShell.init_for_window(window)
        self._set_edge(window)
        GtkLayerShell.set_layer(window, GtkLayerShell.Layer.BOTTOM)
        GtkLayerShell.set_namespace(window, APPLICATION_ID)

    def __init__(self, parent):
        self._parent = parent
        window = self._enquiry("delta > window")
        window.connect("realize", self._on_realize)

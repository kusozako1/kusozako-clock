# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_clock.Entity import DeltaEntity
from .model.Model import DeltaModel
from .header.Header import DeltaHeader
from .content_area.ContentArea import DeltaContentArea


class DeltaTodo(DeltaEntity):

    @classmethod
    def new(cls, parent, item_data):
        instance = cls(parent)
        instance.construct(item_data)

    def _delta_info_gfile(self):
        return self._model.get_gfile()

    def construct(self, item_data):
        self._model = DeltaModel.new(self, item_data)
        DeltaHeader.new(self, item_data)
        DeltaContentArea.new(self, item_data)

    def __init__(self, parent):
        self._parent = parent

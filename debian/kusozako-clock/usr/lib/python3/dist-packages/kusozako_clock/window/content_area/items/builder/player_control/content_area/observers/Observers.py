# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .ProxyAppeared import DeltaProxyAppeared
from .ProxyVanished import DeltaProxyVanished

class EchoObservers:

    def __init__(self, parent):
        DeltaProxyAppeared(parent)
        DeltaProxyVanished(parent)

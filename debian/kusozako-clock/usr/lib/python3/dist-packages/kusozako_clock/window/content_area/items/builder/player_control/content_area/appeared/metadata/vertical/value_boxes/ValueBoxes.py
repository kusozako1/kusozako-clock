# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Title import DeltaTitle
from .Artist import DeltaArtist
from .Album import DeltaAlbum


class EchoValueBoxes:

    def __init__(self, parent):
        DeltaTitle(parent)
        DeltaArtist(parent)
        DeltaAlbum(parent)

# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Button import AlfaButton


class DeltaPlayButton(AlfaButton):

    __icon_name__ = "media-playback-start-symbolic"
    __method_name__ = "Play"

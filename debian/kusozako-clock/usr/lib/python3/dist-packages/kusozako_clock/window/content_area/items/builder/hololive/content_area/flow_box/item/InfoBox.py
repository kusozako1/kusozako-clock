# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Pango
from kusozako_clock.Entity import DeltaEntity


class DeltaInfoBox(Gtk.Box, DeltaEntity):

    @classmethod
    def new(cls, parent, video_entity):
        instance = cls(parent)
        instance.construct(video_entity)

    def construct(self, video_entity):
        title = Gtk.Label(
            video_entity["title"],
            lines=3,
            ellipsize=Pango.EllipsizeMode.END,
            yalign=0.5,
            xalign=0,
            vexpand=True,
            hexpand=False,
            wrap_mode=Pango.WrapMode.WORD_CHAR,
            )
        self.add(title)
        spacer = Gtk.Separator(margin_bottom=2, margin_top=2)
        self.add(spacer)
        talent = video_entity["talent"]["name"]
        if video_entity["isLive"]:
            time = "LIVE"
        else:
            time = video_entity["displayDate"]
        label = "{} / {}".format(talent, time)
        time_label = Gtk.Label(label, lines=1, xalign=0)
        self.add(time_label)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.VERTICAL,
            # margin_start=8,
            hexpand=True,
            vexpand=True,
            )
        self._raise("delta > add to container", self)

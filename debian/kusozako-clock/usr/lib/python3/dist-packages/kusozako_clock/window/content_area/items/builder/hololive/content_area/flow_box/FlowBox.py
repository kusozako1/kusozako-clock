# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gio
from kusozako_clock.Entity import DeltaEntity
from kusozako_clock.const import HololivePages
from .item.Item import CharlieItem


class DeltaFlowBox(Gtk.FlowBox, DeltaEntity):

    def _create_widget_func(self, video_entity):
        return CharlieItem.new(video_entity)

    def _on_child_activated(self, flow_box, flow_box_child):
        index = flow_box_child.get_index()
        model = self._enquiry("delta > model")
        video_entity = model[index]
        Gio.AppInfo.launch_default_for_uri(video_entity["url"], None)

    def __init__(self, parent):
        self._parent = parent
        scrolled_window = Gtk.ScrolledWindow(
            hexpand=True,
            vexpand=True,
            propagate_natural_width=True,
            hscrollbar_policy=Gtk.PolicyType.NEVER,
            )
        Gtk.FlowBox.__init__(
            self,
            max_children_per_line=1,
            activate_on_single_click=False,
            )
        self.connect("child-activated", self._on_child_activated)
        model = self._enquiry("delta > model")
        self.bind_model(model, self._create_widget_func)
        scrolled_window.add(self)
        user_data = scrolled_window, HololivePages.VIEWER
        self._raise("delta > add to stack", user_data)

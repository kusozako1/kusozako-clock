# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .GetMetadata import DeltaGetMetadata
from .GetCoverart import DeltaGetCoverart


class EchoStartupMethods:

    def __init__(self, parent):
        DeltaGetMetadata(parent)
        DeltaGetCoverart(parent)

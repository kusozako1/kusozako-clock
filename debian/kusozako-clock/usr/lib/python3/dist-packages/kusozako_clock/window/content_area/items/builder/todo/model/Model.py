# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from kusozako_clock.Entity import DeltaEntity
from kusozako_clock.const import ItemKeys


class DeltaModel(DeltaEntity):

    @classmethod
    def new(cls, parent, item_data):
        instance = cls(parent)
        instance.construct(item_data)
        return instance

    def _get_cache_path(self):
        names = [GLib.get_user_cache_dir(), "com.gitlab.kusozako1.Clock"]
        directory_path = GLib.build_filenamev(names)
        directory_gfile = Gio.File.new_for_path(directory_path)
        if not directory_gfile.query_exists(None):
            directory_gfile.make_directory(None)
        content_gfile = directory_gfile.get_child("todo.txt")
        content_path = content_gfile.get_path()
        if not content_gfile.query_exists(None):
            content_gfile.create(Gio.FileCreateFlags.NONE, None)
            content_bytes = bytes("Click Edit button to Edit", "utf-8")
            GLib.file_set_contents(content_path, content_bytes)
        return content_path

    def get_gfile(self):
        if not self._gfile.query_exists():
            return None
        return self._gfile

    def construct(self, item_data):
        path = item_data.get(ItemKeys.PATH, "")
        if path.startswith("~"):
            path = path.replace("~", GLib.get_home_dir(), 1)
        if not GLib.file_test(path, GLib.FileTest.EXISTS):
            path = self._get_cache_path()
        self._gfile = Gio.File.new_for_path(path)

    def __init__(self, parent):
        self._parent = parent

# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GdkPixbuf
from kusozako_clock.Entity import DeltaEntity
from kusozako_clock.util.SoupSession import FoxtrotSoupSession
from kusozako_clock.util.StaticImageCache import FoxtrotStaticImageCache


class DeltaImage(Gtk.Image, DeltaEntity):

    @classmethod
    def new(cls, parent, video_entity):
        instance = cls(parent)
        instance.construct(video_entity)

    def _finished(self, data_input_stream):
        pixbuf = GdkPixbuf.Pixbuf.new_from_stream_at_scale(
            data_input_stream,
            180,            # width
            -1,             # height
            True,           # preserve_aspect_ratio
            None,           # Cancellable
            )
        self.set_from_pixbuf(pixbuf)
        data_input_stream.close()

    def _pixbuf_ready(self, pixbuf):
        self.set_from_pixbuf(pixbuf)

    def construct(self, video_entity):
        url = video_entity["thumbnail"]
        if url.startswith("https://schedule-static.hololive.tv/image"):
            static_image_cache = FoxtrotStaticImageCache.get_default()
            static_image_cache.get_async(url, self._pixbuf_ready)
        else:
            soup_session = FoxtrotSoupSession.get_default()
            soup_session.get_async(url, self._finished)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Image.__init__(self, opacity=0.75)
        self._raise("delta > add to container", self)

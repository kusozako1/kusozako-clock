# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import yaml
from gi.repository import GLib
from kusozako_clock.Entity import DeltaEntity
from kusozako_clock.const import HololiveSignals
from kusozako_clock.util.SoupSession import FoxtrotSoupSession

API = "https://schedule.hololive.tv/api/list"


class DeltaParser(DeltaEntity):

    def _timeout(self):
        user_data = HololiveSignals.RELOADING_FINISHED, None
        self._raise("delta > hololive signal", user_data)
        return GLib.SOURCE_REMOVE

    def _parse(self, contents):
        data = yaml.safe_load(contents)
        date_group_list = data["dateGroupList"]
        for date in date_group_list:
            for video in date["videoList"]:
                self._raise("delta > video found", video)
        window = self._enquiry("delta > window")
        window.show_all()
        GLib.timeout_add_seconds(1, self._timeout)

    def _finished(self, data_input_stream):
        contents, _ = data_input_stream.read_upto("\0", -1, None)
        self._parse(contents.encode('utf-8'))

    def parse(self):
        self._soup_session.get_async(API, self._finished)

    def __init__(self, parent):
        self._parent = parent
        self._soup_session = FoxtrotSoupSession.get_default()
        self.parse()

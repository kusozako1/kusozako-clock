# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from kusozako_clock.Entity import DeltaEntity
from kusozako_clock.const import PlayerControlSignals
from . import DBusSpec


class DeltaConnectionWatcher(DeltaEntity):

    def _on_signal(self, proxy, sender_name, signal_name, param):
        if signal_name == "CoverartChanged":
            user_data = PlayerControlSignals.COVERART_CHANGED, param
        elif signal_name == "MetadataChanged":
            user_data = PlayerControlSignals.METADATA_CHANGED, param
        self._raise("delta > player control signal", user_data)

    def _proxy_ready(self, source_object, task, user_data=None):
        proxy = Gio.DBusProxy.new_finish(task)
        self._id = proxy.connect("g-signal", self._on_signal)
        self._raise("delta > set proxy", proxy)
        user_data = PlayerControlSignals.PROXY_APPEARED, None
        self._raise("delta > player control signal", user_data)

    def _on_name_appeared(self, connection, well_known_name, unique_name):
        Gio.DBusProxy.new(
            connection,                         # dbus connection
            Gio.DBusProxyFlags.NONE,            # proxy flags
            None,                               # dbus interface info
            unique_name,                        # bus name
            DBusSpec.OBJECT_PATH,               # object path
            DBusSpec.INTERFACE_NAME,            # interface name
            None,                               # Cancellable
            self._proxy_ready,                  # callback
            None                                # callback data
            )

    def _on_name_vanished(self, connection, well_known_name):
        if self._id is None:
            return
        self._raise("delta > destroy proxy", self._id)
        user_data = PlayerControlSignals.PROXY_VANISHED, None
        self._raise("delta > player control signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        self._id = None
        connection = Gio.bus_get_sync(Gio.BusType.SESSION)
        Gio.bus_watch_name_on_connection(
            connection,
            DBusSpec.WELL_KNOWN_NAME_PLAYER,
            Gio.BusNameWatcherFlags.NONE,
            self._on_name_appeared,
            self._on_name_vanished
            )

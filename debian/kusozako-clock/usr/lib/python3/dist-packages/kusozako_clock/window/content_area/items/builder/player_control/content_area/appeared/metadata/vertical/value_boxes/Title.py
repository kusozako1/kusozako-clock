# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .ValueBox import AlfaValueBox


class DeltaTitle(AlfaValueBox):

    __header__ = "Title : "
    __index__ = 0

# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .PreviousButton import DeltaPreviousButton
from .PauseButton import DeltaPauseButton
from .PlayButton import DeltaPlayButton
from .NextButton import DeltaNextButton


class EchoButtons:

    def __init__(self, parent):
        DeltaPreviousButton(parent)
        DeltaPauseButton(parent)
        DeltaPlayButton(parent)
        DeltaNextButton(parent)

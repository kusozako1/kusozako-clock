# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_clock.Entity import DeltaEntity
from .builder.Builder import DeltaBuilder
from .Timer import FoxtrotTimer


class DeltaItems(DeltaEntity):

    def _delta_call_add_to_timer(self, widget):
        self._timer.append(widget)

    def __init__(self, parent):
        self._parent = parent
        self._timer = FoxtrotTimer()
        DeltaBuilder(self)
        self._timer.start()

# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Pango
from kusozako_clock.Entity import DeltaEntity
from kusozako_clock.const import PlayerControlSignals


class AlfaValueBox(Gtk.Box, DeltaEntity):

    __header__ = "define header here"
    __index__ = 0                       # index for glib_variant_tuple

    def receive_transmission(self, user_data):
        signal, glib_variant = user_data
        if signal != PlayerControlSignals.METADATA_CHANGED:
            return
        self._value_label.set_label(glib_variant[self.__index__])

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        header_label = Gtk.Label(self.__header__)
        self.add(header_label)
        self._value_label = Gtk.Label(
            xalign=1,
            ellipsize=Pango.EllipsizeMode.END,
            hexpand=True,
            )
        self.add(self._value_label)
        self._raise("delta > add to container", self)
        self._raise("delta > register player control object", self)

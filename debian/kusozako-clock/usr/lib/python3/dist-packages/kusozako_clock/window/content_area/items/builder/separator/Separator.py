# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_clock.Entity import DeltaEntity
from kusozako_clock.bravo.Margin import BravoMargin


class DeltaSeparator(Gtk.Separator, DeltaEntity, BravoMargin):

    @classmethod
    def new(cls, parent, item_data):
        instance = cls(parent)
        instance.construct(item_data)

    def construct(self, item_data):
        self._set_margin(item_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Separator.__init__(self)
        self._raise("delta > add to container", self)

# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_clock.Entity import DeltaEntity
from kusozako_clock.const import PlayerControlSignals

ICON_SIZE = Gtk.IconSize.MENU


class AlfaButton(Gtk.Button, DeltaEntity):

    __icon_name__ = "define icon name here."
    __method_name__ = "define method name here."

    def _on_clicked(self, button):
        user_data = PlayerControlSignals.PROXY_METHOD, self.__method_name__
        self._raise("delta > player control signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(
            self,
            relief=Gtk.ReliefStyle.NONE,
            image=Gtk.Image.new_from_icon_name(self.__icon_name__, ICON_SIZE),
            )
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)

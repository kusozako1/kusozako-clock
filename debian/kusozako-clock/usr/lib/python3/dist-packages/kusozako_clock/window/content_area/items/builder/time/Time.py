# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib
from kusozako_clock.Entity import DeltaEntity
from kusozako_clock.const import ItemKeys
from kusozako_clock.bravo.Margin import BravoMargin
from kusozako_clock.bravo.Label import BravoLabel


class DeltaTime(Gtk.Label, DeltaEntity, BravoLabel, BravoMargin):

    @classmethod
    def new(cls, parent, item_data):
        instance = cls(parent)
        instance.construct(item_data)

    def refresh(self):
        date_time = GLib.DateTime.new_now_local()
        pango_markup = date_time.format(self._format)
        self.set_markup(pango_markup)

    def construct(self, item_data):
        self._set_margin(item_data)
        self._label_construction(item_data)
        self._format = item_data[ItemKeys.FORMAT]

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self)
        self._raise("delta > add to container", self)
        self._raise("delta > add to timer", self)

# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_clock.Entity import DeltaEntity
from .value_boxes.ValueBoxes import EchoValueBoxes


class DeltaVertical(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.VERTICAL,
            hexpand=False,
            margin_start=8,
            margin_end=8,
            margin_top=8,
            spacing=4
            )
        EchoValueBoxes(self)
        self._raise("delta > add to container", self)

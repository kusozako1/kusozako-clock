# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_clock.Entity import DeltaEntity
from kusozako_clock.const import HololiveSignals
from kusozako_clock.const import HololivePages
from kusozako_clock.bravo.Margin import BravoMargin
from kusozako_clock.util.StaticImageCache import FoxtrotStaticImageCache
from .model.Model import DeltaModel
from .flow_box.FlowBox import DeltaFlowBox
from .NowLoading import DeltaNowLoading


class DeltaContentArea(Gtk.Stack, DeltaEntity, BravoMargin):

    @classmethod
    def new(cls, parent, item_data):
        instance = cls(parent)
        instance.construct(item_data)

    def _delta_info_model(self):
        return self._model

    def _delta_call_add_to_stack(self, user_data):
        widget, name = user_data
        self.add_named(widget, name)

    def receive_transmission(self, user_data):
        signal, _ = user_data
        if signal == HololiveSignals.RELOADING_STARTED:
            self.set_visible_child_name(HololivePages.NOW_LOADING)
        elif signal == HololiveSignals.RELOADING_FINISHED:
            self.set_visible_child_name(HololivePages.VIEWER)

    def construct(self, item_data):
        self._set_margin(item_data)

    def __init__(self, parent):
        self._parent = parent
        self._model = DeltaModel(self)
        FoxtrotStaticImageCache.init()
        Gtk.Stack.__init__(
            self,
            transition_duration=1000,
            transition_type=Gtk.StackTransitionType.CROSSFADE,
            )
        DeltaNowLoading(self)
        DeltaFlowBox(self)
        self._raise("delta > register hololive object", self)
        self._raise("delta > add to container", self)

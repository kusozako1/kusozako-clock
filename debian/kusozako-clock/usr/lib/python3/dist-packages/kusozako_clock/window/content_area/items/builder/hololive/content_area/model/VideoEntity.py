# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GObject


class FoxtrotVideoEntity(GObject.Object):

    def __getitem__(self, key):
        return self._data[key]

    def __init__(self, data):
        GObject.Object.__init__(self)
        self._data = data

# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import importlib
from kusozako_clock.Entity import DeltaEntity
from kusozako_clock.const import ConfigKeys
from kusozako_clock.const import ItemKeys
from kusozako_clock.config.Config import KusozakoConfig


class DeltaBuilder(DeltaEntity):

    def _get_camel_case(self, type_name):
        camel_case = ""
        for element in type_name.split("-"):
            camel_case += element.capitalize()
        return camel_case

    def _build(self, item_data):
        type_name = item_data.pop(ItemKeys.TYPE)
        snake_case = type_name.replace("-", "_")
        camel_case = self._get_camel_case(type_name)
        module_name = "{}.{}.{}".format(__package__, snake_case, camel_case)
        class_name = "Delta{}".format(camel_case)
        module = importlib.import_module(module_name)
        class_ = getattr(module, class_name)
        class_.new(self, item_data)

    def __init__(self, parent):
        self._parent = parent
        config = KusozakoConfig.get_default()
        for item_data in config[ConfigKeys.ITEMS]:
            self._build(item_data)

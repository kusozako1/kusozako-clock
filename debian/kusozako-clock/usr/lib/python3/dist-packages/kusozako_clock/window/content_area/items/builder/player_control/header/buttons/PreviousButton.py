# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Button import AlfaButton


class DeltaPreviousButton(AlfaButton):

    __icon_name__ = "media-seek-backward-symbolic"
    __method_name__ = "Previous"

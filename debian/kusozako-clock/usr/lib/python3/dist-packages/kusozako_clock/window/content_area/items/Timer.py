# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako_clock.const import ConfigKeys
from kusozako_clock.config.Config import KusozakoConfig


class FoxtrotTimer:

    def _timeout(self):
        for item in self._items:
            item.refresh()
        return GLib.SOURCE_CONTINUE

    def append(self, item):
        self._items.append(item)

    def start(self):
        config = KusozakoConfig.get_default()
        interval = config[ConfigKeys.INTERVAL]
        GLib.timeout_add_seconds(interval, self._timeout)

    def __init__(self):
        self._items = []

# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_clock.Entity import DeltaEntity
from kusozako_clock.bravo.Margin import BravoMargin
from .HeaderLabel import DeltaHeaderLabel
from .ValueLabel import DeltaValueLabel


class AlfaKeyValueBox(Gtk.Box, DeltaEntity, BravoMargin):

    __header_title__ = "define header label here."

    @classmethod
    def new(cls, parent, item_data):
        instance = cls(parent)
        instance.construct(item_data)

    def _get_value(self):
        raise NotImplementedError

    def _delta_info_header_title(self):
        return self.__header_title__

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def _on_construct(self, item_data):
        pass

    def refresh(self):
        text = self._get_value()
        self._value_label.set_label(text)

    def construct(self, item_data):
        self._on_construct(item_data)
        self._set_margin(item_data)
        self._header_label = DeltaHeaderLabel.new(self, item_data)
        self._value_label = DeltaValueLabel.new(self, item_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        self._raise("delta > add to container", self)
        self._raise("delta > add to timer", self)

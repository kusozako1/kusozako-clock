# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

REQUEST_RELOAD = "request-reload"           # None
RELOADING_STARTED = "reloading-started"     # None
RELOADING_FINISHED = "reloading-finished"   # None

# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later


DEFAULT = """
{
    "width": 320,
    "height": 738,
    "edge": ["top", "right"],
    "foreground-color": "White",
    "shadow-color": "DeepPink",
    "shadow-type": "Blur",
    "background-color": "Transparent",
    "interval": 2,
    "items": [
        {
            "type": "time",
            "align": "right",
            "font-size": 48,
            "font-weight": 100,
            "format": "%R"
        },
        {
            "type": "time",
            "align": "right",
            "font-size": 14,
            "format": "%F (%a)"
        },
        {
            "type": "label",
            "title": "SYSTEM",
            "align": "left",
            "margin-top": 16
        },
        {
            "type": "separator",
            "margin-top": 4
        },
        {
            "type": "cpu",
            "margin-top": 8
        },
        {
            "type": "memory",
            "margin-top": 4
        },
        {
            "type": "todo",
            "margin-top": 8
        }
    ]
}
"""

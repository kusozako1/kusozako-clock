# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib

OUTLINE = """
        -1px -1px @kusozako_shadow_color,
        -1px 1px @kusozako_shadow_color,
        1px -1px @kusozako_shadow_color,
        1px 1px @kusozako_shadow_color;
"""

BLUR = """
        3px 3px 5px @kusozako_shadow_color,
        -3px -3px 5px @kusozako_shadow_color,
        3px -3px 5px @kusozako_shadow_color,
        -3px 3px 5px @kusozako_shadow_color;
"""


def get_css(shadow_type=None):
    if shadow_type is None:
        return BLUR
    shadow_type_uppercase = GLib.utf8_strup(shadow_type, -1)
    if shadow_type_uppercase == "OUTLINE":
        return OUTLINE
    return BLUR

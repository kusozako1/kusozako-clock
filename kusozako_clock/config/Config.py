# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import json
from gi.repository import Gio
from gi.repository import GLib
from . import Path
from .Default import DEFAULT


class KusozakoConfig:

    @classmethod
    def init(cls, config_path=None):
        cls._default = cls(config_path)

    @classmethod
    def get_default(cls):
        return cls._default

    def _ensure_directory(self):
        gfile = Gio.File.new_for_path(Path.DIRECTORY_PATH)
        if not gfile.query_exists(None):
            gfile.make_directory(None)

    def _ensure_file(self):
        gfile = Gio.File.new_for_path(Path.FILE_PATH)
        if not gfile.query_exists(None):
            bytes_data = bytes(DEFAULT, "utf-8")
            GLib.file_set_contents(Path.FILE_PATH, bytes_data)
        _, contents, _ = gfile.load_contents(None)
        return contents.decode("utf-8")

    def _get_config_path(self, config=None):
        if config is None:
            return None
        if GLib.file_test(config, GLib.FileTest.EXISTS):
            return config
        path = GLib.build_filenamev([Path.DIRECTORY_PATH, config])
        return path if GLib.file_test(path, GLib.FileTest.EXISTS) else None

    def __getitem__(self, key):
        return self._config.get(key, None)

    def __init__(self, config=None):
        path = self._get_config_path(config)
        if path is not None:
            _, contents = GLib.file_get_contents(path)
            self._config = json.loads(contents)
        else:
            self._ensure_directory()
            contents = self._ensure_file()
            self._config = json.loads(contents)

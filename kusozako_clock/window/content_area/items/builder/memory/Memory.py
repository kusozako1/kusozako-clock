# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import psutil
from gi.repository import GLib
from kusozako_clock.alfa.key_value_box.KeyValueBox import AlfaKeyValueBox


class DeltaMemory(AlfaKeyValueBox):

    __header_title__ = "Memory:"

    def _get_value(self):
        data = psutil.virtual_memory()
        used = GLib.format_size_full(data.used, GLib.FormatSizeFlags.DEFAULT)
        total = GLib.format_size_full(data.total, GLib.FormatSizeFlags.DEFAULT)
        return "{} / {}".format(used, total)

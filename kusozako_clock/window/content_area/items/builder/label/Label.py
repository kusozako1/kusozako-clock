# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_clock.Entity import DeltaEntity
from kusozako_clock.const import ItemKeys
from kusozako_clock.bravo.Margin import BravoMargin
from kusozako_clock.bravo.Label import BravoLabel


class DeltaLabel(Gtk.Label, DeltaEntity, BravoLabel, BravoMargin):

    @classmethod
    def new(cls, parent, item_data):
        instance = cls(parent)
        instance.construct(item_data)

    def construct(self, item_data):
        self._set_margin(item_data)
        self._label_construction(item_data)
        markup = item_data.get(ItemKeys.TITLE, "")
        self.set_markup(markup)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self, margin_start=8, margin_end=8)
        self._raise("delta > add to container", self)

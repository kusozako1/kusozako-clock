# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import psutil
from kusozako_clock.alfa.key_value_box.KeyValueBox import AlfaKeyValueBox


class DeltaCpu(AlfaKeyValueBox):

    __header_title__ = "CPU:"

    def _get_value(self):
        cpu_percent = psutil.cpu_percent(interval=None)
        cpu_freq = round(psutil.cpu_freq().current/1000, 2)
        value = "{} % / {} GHz".format(cpu_percent, cpu_freq)
        return value

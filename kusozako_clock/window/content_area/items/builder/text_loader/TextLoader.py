# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gio
from gi.repository import GLib
from kusozako_clock.Entity import DeltaEntity
from kusozako_clock.const import ItemKeys
from kusozako_clock.bravo.Margin import BravoMargin
from kusozako_clock.bravo.Label import BravoLabel


class DeltaTextLoader(Gtk.Label, DeltaEntity, BravoLabel, BravoMargin):

    @classmethod
    def new(cls, parent, item_data):
        instance = cls(parent)
        instance.construct(item_data)

    def _get_text(self):
        if not self._gfile.query_exists():
            return "TEXT FILE NOT FOUND"
        _, contents, _ = self._gfile.load_contents(None)
        return contents.decode("utf8")

    def refresh(self):
        text = self._get_text()
        self.set_label(text)

    def construct(self, item_data):
        self._set_margin(item_data)
        self._label_construction(item_data)
        path = item_data[ItemKeys.PATH]
        if path.startswith("~"):
            path = path.replace("~", GLib.get_home_dir(), 1)
        self._gfile = Gio.File.new_for_path(path)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self)
        self._raise("delta > add to container", self)
        self._raise("delta > add to timer", self)

# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_clock.Entity import DeltaEntity
from kusozako_clock.bravo.Margin import BravoMargin
from kusozako_clock.bravo.Label import BravoLabel


class DeltaContentArea(Gtk.Label, DeltaEntity, BravoLabel, BravoMargin):

    @classmethod
    def new(cls, parent, item_data):
        instance = cls(parent)
        instance.construct(item_data)

    def refresh(self):
        if self._gfile is None:
            self.set_label("CAN'T LOAD CONTENT")
        else:
            _, contents, _ = self._gfile.load_contents(None)
            text = contents.decode("utf8")
            self.set_label(text)

    def construct(self, item_data):
        self._set_margin(item_data)
        self._label_construction(item_data)
        self._gfile = self._enquiry("delta > gfile")

    def __init__(self, parent):
        self._parent = parent
        scrolled_window = Gtk.ScrolledWindow(
            hexpand=True,
            vexpand=True,
            hscrollbar_policy=Gtk.PolicyType.NEVER,
            )
        Gtk.Label.__init__(self, yalign=0, margin_start=8, margin_end=8)
        scrolled_window.add(self)
        self._raise("delta > add to container", scrolled_window)
        self._raise("delta > add to timer", self)

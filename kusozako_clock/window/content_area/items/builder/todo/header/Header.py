# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_clock.Entity import DeltaEntity
from kusozako_clock.bravo.Margin import BravoMargin
from .EditButton import DeltaEditButton


class DeltaHeader(Gtk.Box, DeltaEntity, BravoMargin):

    @classmethod
    def new(cls, parent, item_data):
        instance = cls(parent)
        instance.construct(item_data)

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def construct(self, item_data):
        self._set_margin(item_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(
            self,
            hexpand=True,
            orientation=Gtk.Orientation.HORIZONTAL
            )
        self.set_size_request(-1, 32)
        label = Gtk.Label(
            "TODO",
            xalign=0,
            hexpand=True,
            margin_start=8,
            margin_end=8
            )
        self.add(label)
        DeltaEditButton(self)
        self._raise("delta > add to container", self)
        self._raise("delta > add to container", Gtk.Separator())

# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gio
from kusozako_clock.Entity import DeltaEntity

ICON_NAME = "document-edit-symbolic"
ICON_SIZE = Gtk.IconSize.MENU


class DeltaEditButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        gfile = self._enquiry("delta > gfile")
        if gfile is None:
            return
        app_info = Gio.AppInfo.get_default_for_type("text/plain", True)
        app_info.launch([gfile], None)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(
            self,
            relief=Gtk.ReliefStyle.NONE,
            image=Gtk.Image.new_from_icon_name(ICON_NAME, ICON_SIZE),
            )
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)

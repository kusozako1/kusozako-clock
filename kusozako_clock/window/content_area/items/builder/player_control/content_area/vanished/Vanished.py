# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_clock.Entity import DeltaEntity


class DeltaVanished(Gtk.Label, DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self, "kusozako-music not activated")
        self.set_size_request(-1, 48)
        user_data = self, "vanished"
        self._raise("delta > add to stack", user_data)

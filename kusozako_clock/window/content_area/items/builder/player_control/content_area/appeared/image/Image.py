# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib
from gi.repository import GdkPixbuf
from kusozako_clock.Entity import DeltaEntity
from kusozako_clock.const import PlayerControlSignals


class DeltaImage(Gtk.Image, DeltaEntity):

    @classmethod
    def new(cls, parent, item_data):
        instance = cls(parent)
        instance.construct(item_data)

    def _get_pixbuf(self, glib_variant):
        path = glib_variant[0]
        if not GLib.file_test(path, GLib.FileTest.EXISTS):
            return None
        pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(
            path,
            self._width,
            self._height,
            True,
            )
        self.set_size_request(pixbuf.get_width(), pixbuf.get_height())
        return pixbuf

    def receive_transmission(self, user_data):
        signal, glib_variant = user_data
        if signal != PlayerControlSignals.COVERART_CHANGED:
            return
        pixbuf = self._get_pixbuf(glib_variant)
        self.set_from_pixbuf(pixbuf)

    def construct(self, item_data):
        self._width = item_data.get("coverart-width", -1)
        self._height = item_data.get("coverart-height", -1)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Image.__init__(
            self,
            margin_top=8,
            margin_start=4,
            margin_end=0,
            )
        self._raise("delta > add to container", self)
        self._raise("delta > register player control object", self)

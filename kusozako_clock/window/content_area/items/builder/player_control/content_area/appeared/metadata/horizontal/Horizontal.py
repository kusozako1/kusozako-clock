# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_clock.Entity import DeltaEntity
from .Title import DeltaTitle
from .Separator import DeltaSeparator
from .AdditionalData import DeltaAdditionalData


class DeltaHorizontal(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.VERTICAL,
            vexpand=False,
            )
        DeltaTitle(self)
        DeltaSeparator(self)
        DeltaAdditionalData(self)
        self._raise("delta > add to container", self)

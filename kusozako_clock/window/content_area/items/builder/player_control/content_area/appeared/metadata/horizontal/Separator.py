# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_clock.Entity import DeltaEntity


class DeltaSeparator(Gtk.Separator, DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        Gtk.Separator.__init__(self, hexpand=True)
        self._raise("delta > add to container", self)

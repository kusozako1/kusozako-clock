# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .ValueBox import AlfaValueBox


class DeltaAlbum(AlfaValueBox):

    __header__ = "Album : "
    __index__ = 2

# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Pango
from kusozako_clock.Entity import DeltaEntity
from kusozako_clock.const import PlayerControlSignals


class DeltaAdditionalData(Gtk.Label, DeltaEntity):

    def receive_transmission(self, user_data):
        signal, glib_variant = user_data
        if signal != PlayerControlSignals.METADATA_CHANGED:
            return
        label = "{}/{}".format(glib_variant[1], glib_variant[2])
        self.set_label(label)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(
            self,
            lines=1,
            ellipsize=Pango.EllipsizeMode.END,
            yalign=0.5,
            xalign=0,
            vexpand=False,
            hexpand=False,
            wrap_mode=Pango.WrapMode.WORD_CHAR,
            margin_bottom=4,
            )
        self._raise("delta > register player control object", self)
        self._raise("delta > add to container", self)

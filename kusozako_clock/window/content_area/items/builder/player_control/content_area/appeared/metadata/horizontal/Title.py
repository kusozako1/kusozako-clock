# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Pango
from kusozako_clock.Entity import DeltaEntity
from kusozako_clock.const import PlayerControlSignals


class DeltaTitle(Gtk.Label, DeltaEntity):

    def receive_transmission(self, user_data):
        signal, glib_variant = user_data
        if signal != PlayerControlSignals.METADATA_CHANGED:
            return
        self.set_label(glib_variant[0])

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(
            self,
            lines=3,
            ellipsize=Pango.EllipsizeMode.END,
            yalign=0.5,
            xalign=0,
            vexpand=True,
            hexpand=False,
            wrap_mode=Pango.WrapMode.WORD_CHAR,
            )
        self._raise("delta > register player control object", self)
        self._raise("delta > add to container", self)

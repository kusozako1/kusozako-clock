# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_clock.Entity import DeltaEntity
from kusozako_clock.const import PlayerControlSignals


class DeltaProxyAppeared(DeltaEntity):

    def receive_transmission(self, user_data):
        signal, _ = user_data
        if signal != PlayerControlSignals.PROXY_APPEARED:
            return
        self._raise("delta > set stack page", "appeared")

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register player control object", self)

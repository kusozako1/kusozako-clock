# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_clock.Entity import DeltaEntity
from .vanished.Vanished import DeltaVanished
from .appeared.Appeared import DeltaAppeared
from .observers.Observers import EchoObservers


class DeltaContentArea(Gtk.Stack, DeltaEntity):

    @classmethod
    def new(cls, parent, item_data):
        instance = cls(parent)
        instance.construct(item_data)

    def _delta_call_add_to_stack(self, user_data):
        widget, name = user_data
        self.add_named(widget, name)

    def _delta_call_set_stack_page(self, name):
        self.set_visible_child_name(name)

    def construct(self, item_data):
        DeltaVanished(self)
        DeltaAppeared.new(self, item_data)
        EchoObservers(self)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Stack.__init__(
            self,
            transition_type=Gtk.StackTransitionType.CROSSFADE,
            vhomogeneous=False,
            )
        self._raise("delta > add to container", self)

# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Button import AlfaButton


class DeltaNextButton(AlfaButton):

    __icon_name__ = "media-seek-forward-symbolic"
    __method_name__ = "Next"

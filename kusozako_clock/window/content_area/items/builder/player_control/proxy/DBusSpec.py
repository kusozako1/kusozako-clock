# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

WELL_KNOWN_NAME_PLAYER = "com.gitlab.kusozako1.Music.Control"
OBJECT_PATH = "/com/gitlab/kusozako1/Music/Control"
INTERFACE_NAME = "com.gitlab.kusozako1.Music.Control"

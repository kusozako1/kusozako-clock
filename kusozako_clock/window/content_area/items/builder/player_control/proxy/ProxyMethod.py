# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_clock.Entity import DeltaEntity
from kusozako_clock.const import PlayerControlSignals


class DeltaProxyMethod(DeltaEntity):

    def receive_transmission(self, user_data):
        signal, method_name = user_data
        if signal != PlayerControlSignals.PROXY_METHOD:
            return
        user_data = method_name, None
        self._raise("delta > call method", user_data)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register player control object", self)

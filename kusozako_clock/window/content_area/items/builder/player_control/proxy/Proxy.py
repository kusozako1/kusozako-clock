# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from kusozako_clock.Entity import DeltaEntity
from .ConnectionWatcher import DeltaConnectionWatcher
from .ProxyMethod import DeltaProxyMethod
from .startup_methods.StartupMethods import EchoStartupMethods


class DeltaProxy(DeltaEntity):

    def _method_finished(self, proxy, task, callback=None):
        glib_variant = proxy.call_finish(task)
        if callback is not None:
            callback(glib_variant)

    def _delta_call_set_proxy(self, proxy):
        self._proxy = proxy

    def _delta_call_destroy_proxy(self, id_):
        self._proxy.disconnect(id_)

    def _delta_call_call_method(self, user_data):
        if self._proxy is None:
            return
        method_name, callback = user_data
        self._proxy.call(
            method_name,
            None,                               # method param
            Gio.DBusCallFlags.NO_AUTO_START,    # flags
            -1,                                 # timeout. -1 means default.
            None,                               # Cacellable
            self._method_finished,              # callback
            callback                            # data for _method_finished
            )

    def __init__(self, parent):
        self._parent = parent
        self._proxy = None
        DeltaConnectionWatcher(self)
        DeltaProxyMethod(self)
        EchoStartupMethods(self)

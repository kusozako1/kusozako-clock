# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import psutil
from kusozako_clock.alfa.key_value_box.KeyValueBox import AlfaKeyValueBox


class DeltaSwap(AlfaKeyValueBox):

    __header_title__ = "Swap:"

    def _get_value(self):
        data = psutil.swap_memory()
        used = round(data.used / (2**30), 2)
        total = round(data.total / (2**30), 2)
        return "{} GiB / {} GiB".format(used, total)

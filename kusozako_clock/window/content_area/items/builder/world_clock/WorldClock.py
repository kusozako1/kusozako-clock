# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako_clock.alfa.key_value_box.KeyValueBox import AlfaKeyValueBox


class DeltaWorldClock(AlfaKeyValueBox):

    __header_title__ = ""

    def _on_construct(self, item_data):
        self._format = item_data.get("format", "%R")
        time_zone_identifier = item_data.get("time-zone", "UTC")
        self._time_zone = GLib.TimeZone.new_identifier(time_zone_identifier)
        now = GLib.DateTime.new_now(self._time_zone)
        self.__header_title__ = now.get_timezone_abbreviation()

    def _get_value(self):
        now = GLib.DateTime.new_now(self._time_zone)
        return now.format(self._format), now.get_timezone_abbreviation()

    def refresh(self):
        text, timezone_abbreviation = self._get_value()
        self._value_label.set_label(text)
        self._header_label.set_label(timezone_abbreviation)

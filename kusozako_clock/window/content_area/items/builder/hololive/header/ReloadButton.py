# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_clock.Entity import DeltaEntity
from kusozako_clock.const import HololiveSignals

ICON_NAME = "view-refresh-symbolic"
ICON_SIZE = Gtk.IconSize.MENU


class DeltaReloadButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        user_data = HololiveSignals.REQUEST_RELOAD, None
        self._raise("delta > hololive signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(
            self,
            relief=Gtk.ReliefStyle.NONE,
            image=Gtk.Image.new_from_icon_name(ICON_NAME, ICON_SIZE),
            )
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)

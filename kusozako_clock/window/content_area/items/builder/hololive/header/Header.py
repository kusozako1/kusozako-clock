# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_clock.Entity import DeltaEntity
from .ReloadButton import DeltaReloadButton


class DeltaHeader(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(
            self,
            hexpand=True,
            orientation=Gtk.Orientation.HORIZONTAL,
            margin_start=8,
            margin_end=8,
            )
        self.set_size_request(-1, 32)
        label = Gtk.Label("Holodule", xalign=0, hexpand=True)
        self.add(label)
        DeltaReloadButton(self)
        self._raise("delta > add to container", self)
        self._raise("delta > add to container", Gtk.Separator())

# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_clock.Entity import DeltaEntity
from .Image import DeltaImage
from .InfoBox import DeltaInfoBox


class CharlieItem(Gtk.Box, DeltaEntity):

    @classmethod
    def new(cls, video_entity):
        instance = cls()
        instance.construct(video_entity)
        return instance

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def construct(self, video_entity):
        DeltaImage.new(self, video_entity)
        DeltaInfoBox.new(self, video_entity)

    def __init__(self):
        self._parent = None
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.HORIZONTAL,
            hexpand=True,
            spacing=8,
            )

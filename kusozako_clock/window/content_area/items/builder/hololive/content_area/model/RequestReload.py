# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_clock.Entity import DeltaEntity
from kusozako_clock.const import HololiveSignals


class DeltaRequestReload(DeltaEntity):

    def receive_transmission(self, user_data):
        signal, _ = user_data
        if signal != HololiveSignals.REQUEST_RELOAD:
            return
        self._raise("delta > request reload")

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register hololive object", self)

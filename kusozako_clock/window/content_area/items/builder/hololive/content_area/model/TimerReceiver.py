# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako_clock.Entity import DeltaEntity


class DeltaTimerReceiver(DeltaEntity):

    def refresh(self):
        self._count += 1
        if 100 > self._count:
            return
        date_time = GLib.DateTime.new_now_local()
        minute = date_time.get_minute()
        if minute % 10 == 0:
            self._count = 0
            self._raise("delta > request reload")

    def __init__(self, parent):
        self._parent = parent
        self._count = 0
        self._raise("delta > add to timer", self)

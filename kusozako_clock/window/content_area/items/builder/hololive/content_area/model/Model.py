# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from gi.repository import Gio
from kusozako_clock.Entity import DeltaEntity
from kusozako_clock.const import HololiveSignals
from .TimerReceiver import DeltaTimerReceiver
from .Parser import DeltaParser
from .VideoEntity import FoxtrotVideoEntity
from .RequestReload import DeltaRequestReload


class DeltaModel(Gio.ListStore, DeltaEntity):

    def _is_valid(self, video):
        data = video["datetime"]
        data = data.replace("/", "-")
        data = data.replace(" ", "T")
        data += "+09:00"
        date_time = GLib.DateTime.new_from_iso8601(data)
        now = GLib.DateTime.new_now_utc()
        if now.difference(date_time)/1000000 > 300 and not video["isLive"]:
            return False
        return True

    def _delta_call_video_found(self, video):
        if not self._is_valid(video):
            return
        video_entity = FoxtrotVideoEntity(video)
        self.append(video_entity)

    def _delta_call_request_reload(self):
        user_data = HololiveSignals.RELOADING_STARTED, None
        self._raise("delta > hololive signal", user_data)
        self.remove_all()
        self._parser.parse()

    def __init__(self, parent):
        self._parent = parent
        Gio.ListStore.__init__(self, item_type=FoxtrotVideoEntity)
        self._parser = DeltaParser(self)
        DeltaTimerReceiver(self)
        DeltaRequestReload(self)

# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import io
from PIL import Image
from gi.repository import Gio
from gi.repository import GLib
from gi.repository import GdkPixbuf


class FoxtrotStaticImageCache:

    @classmethod
    def init(cls):
        cls._default = cls()

    @classmethod
    def get_default(cls):
        return cls._default

    def _download_static_image(self, url, callback):
        # cover corp uses .jpg suffix for both webp and jpeg
        gfile = Gio.File.new_for_uri(url)
        glib_bytes, _ = gfile.load_bytes()
        bytes_ = glib_bytes.get_data()
        web_image = Image.open(io.BytesIO(bytes_))
        width, height = web_image.size
        pixbuf_glib_bytes = GLib.Bytes.new(web_image.tobytes())
        pixbuf = GdkPixbuf.Pixbuf.new_from_bytes(
            pixbuf_glib_bytes,          # image data in 8-bit per sample
            GdkPixbuf.Colorspace.RGB,   # color space.
            False,                      # has alpha
            8,                          # bits per sample
            width,                      # width in pixel
            height,                     # height in pixel
            width*3                     # distance in bytes between row
            )
        ratio = 180 / width
        scaled_pixbuf = pixbuf.scale_simple(
            int(width*ratio),
            int(height*ratio),
            GdkPixbuf.InterpType.BILINEAR,
            )
        self._cache[url] = scaled_pixbuf
        callback(scaled_pixbuf)

    def get_async(self, url, callback):
        if url in self._cache:
            callback(self._cache[url])
        GLib.idle_add(self._download_static_image, url, callback)

    def __init__(self):
        self._cache = {}

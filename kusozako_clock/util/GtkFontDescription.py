# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Pango


def get_description(relative_size=0, weight=Pango.Weight.NORMAL):
    settings = Gtk.Settings.get_default()
    font = settings.get_property("gtk-font-name")
    font_description = Pango.font_description_from_string(font)
    absolute_size = font_description.get_size()/Pango.SCALE
    font_description.set_size((absolute_size+relative_size)*Pango.SCALE)
    font_description.set_weight(weight)
    return font_description

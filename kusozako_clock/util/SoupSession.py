
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from gi.repository import Gio
from gi.repository import Soup


class FoxtrotSoupSession:

    @classmethod
    def get_default(cls):
        if "_default" not in dir(cls):
            cls._default = cls()
        return cls._default

    def _on_finished(self, session, task, callback):
        input_stream = session.send_finish(task)
        if input_stream:
            data_input_stream = Gio.DataInputStream.new(input_stream)
            callback(data_input_stream)
        else:
            print("soup session error")

    def get_async(self, uri, callback):
        message = Soup.Message.new("GET", uri)
        self._session.send_async(
            message,                        # message
            GLib.PRIORITY_DEFAULT_IDLE,     # priority
            None,                           # cancellable
            self._on_finished,              # async callback for finished
            callback                        # callback data
            )

    def __init__(self):
        self._session = Soup.Session()
        self._session.set_property("user-agent", "kusozako-soup-session")

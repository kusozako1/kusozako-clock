# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_clock.const import ItemKeys

MARGIN_KEYS = (ItemKeys.MARGIN_TOP, ItemKeys.MARGIN_BOTTOM)


class BravoMargin:

    def _set_margin(self, item_data):
        for key, value in item_data.items():
            if key not in MARGIN_KEYS:
                continue
            self.set_property(key, int(value))

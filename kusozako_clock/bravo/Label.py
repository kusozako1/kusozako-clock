# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Pango
from kusozako_clock.const import ItemKeys

ALIGN = {
    "left": 0,
    "center": 0.5,
    "right": 1,
}


class BravoLabel:

    def _set_pango_attributes(self, item_data):
        font_description = Pango.FontDescription()
        weight = item_data.get(ItemKeys.FONT_WEIGHT, None)
        if weight is not None:
            font_description.set_weight(weight)
        size = item_data.get(ItemKeys.FONT_SIZE, None)
        if size is not None:
            font_description.set_size(size*Pango.SCALE)
        font_description_attribute = Pango.AttrFontDesc.new(font_description)
        attributes = Pango.AttrList()
        attributes.insert(font_description_attribute)
        self.set_attributes(attributes)

    def _set_align(self, align=None):
        if align is None:
            return
        xalign = ALIGN.get(align, align)
        self.set_xalign(xalign)

    def _label_construction(self, item_data):
        self._set_align(item_data.get(ItemKeys.ALIGN))
        self._set_pango_attributes(item_data)

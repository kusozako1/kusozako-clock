# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

TYPE = "type"
ALIGN = "align"
FONT_SIZE = "font-size"
FONT_WEIGHT = "font-weight"
FORMAT = "format"
TITLE = "title"
MARGIN_TOP = "margin-top"
MARGIN_BOTTOM = "margin-bottom"
MARGIN_START = "margin-start"
MARGIN_END = "margin-end"
PATH = "path"

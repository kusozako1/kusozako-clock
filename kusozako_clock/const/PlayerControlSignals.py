# (c) copyright 2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

PROXY_APPEARED = "proxy-appeared"               # None
PROXY_VANISHED = "proxy-vanished"               # None
PROXY_METHOD = "proxy-method"                   # method_name as str
COVERART_CHANGED = "coverart-changed"           # GLib.Variant
METADATA_CHANGED = "metadata-changed"           # GLib.Variant

# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

FOREGROUND_COLOR = "foreground-color"
BACKGROUND_COLOR = "background-color"
SHADOW_COLOR = "shadow-color"
SHADOW_TYPE = "shadow-type"
WIDTH = "width"
HEIGHT = "height"
EDGE = "edge"
INTERVAL = "interval"
ITEMS = "items"

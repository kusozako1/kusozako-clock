# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import gi

gi.require_version('Gtk', '3.0')
gi.require_version('PangoCairo', '1.0')
gi.require_version('GtkLayerShell', '0.1')
gi.require_version('Soup', '3.0')

APPLICATION_ID = "com.gitlab.kusozako1.Clock"

# Kusozako Clock

(almost) text based desktop clock and widget like conky.

This software works on wlroots based wayland compositors.

![screemshot](screenshot/screenshot_1.png)

# License

This software is licensed under GPLv3 or any later version.

See LICENSE.md for more detail.
